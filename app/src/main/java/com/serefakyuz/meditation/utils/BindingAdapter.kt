package com.serefakyuz.meditation.utils

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.facebook.drawee.view.SimpleDraweeView
import com.serefakyuz.meditation.repository.network.Status

/**
 * Created by seref.akyuz on 7/30/2021
 */

const val CLIENT_DATE_FORMAT = "MM/dd/yyyy ddd"

@BindingAdapter("setDate")
fun setDate(view: TextView, timestamp: Long){
    view.text = DateUtils.getFormattedDate(timestamp)
}

@BindingAdapter("setProgressVisibility")
fun setProgressVisibility(progressBar: ProgressBar, status: Status?) {
    progressBar.visibility = if (status == Status.LOADING) View.VISIBLE else View.GONE
}

@BindingAdapter("loadImage")
fun loadImage(imageView: SimpleDraweeView, url: String?) {
    imageView.setImageURI(url)
}
