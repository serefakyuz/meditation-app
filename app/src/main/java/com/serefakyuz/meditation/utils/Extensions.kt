package com.serefakyuz.meditation.utils

import android.content.Context
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.textfield.TextInputEditText
import com.serefakyuz.meditation.R
import com.serefakyuz.meditation.repository.local.PreferenceManager
import com.serefakyuz.meditation.ui.BaseFragment
import com.serefakyuz.meditation.ui.login.LoginActivity

/**
 * Created by seref.akyuz on 7/29/2021
 */
class Extensions {
    fun AppCompatActivity.showNetworkErrorDialog() = showNoNetworkDialog(this)
    fun Fragment.showNetworkErrorDialog() = showNoNetworkDialog(requireContext())

    fun TextInputEditText.clearText() = text.toString().trim()


    fun showNoNetworkDialog(context: Context){
        MaterialDialog(context).show {
            title(R.string.networkErrorTitle)
            message ( R.string.networkErrorMsg, null, null )
            positiveButton ( R.string.ok )
            cancelable(false)
        }
    }

    fun Fragment.unauthorizedEvent(){
        Toast.makeText(requireContext(), R.string.unauthorized, Toast.LENGTH_LONG).show()
        LoginActivity.startActivity(requireContext())
        activity?.finish()
    }

}