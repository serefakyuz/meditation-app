package com.serefakyuz.meditation.utils

import android.text.format.DateFormat
import java.util.*

/**
 * Created by seref.akyuz on 7/30/2021
 */
object DateUtils {

    const val CLIENT_DATE_FORMAT = "MM/dd/yyyy EEE"
    fun getFormattedDate(ts: Long): String{
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = ts
        return DateFormat.format(CLIENT_DATE_FORMAT, calendar).toString()
    }
}