package com.serefakyuz.meditation.di

import android.content.Context
import com.serefakyuz.meditation.BuildConfig
import com.serefakyuz.meditation.repository.local.PreferenceManager
import com.serefakyuz.meditation.repository.network.MeditationApi
import com.serefakyuz.meditation.repository.network.ResponseHandler
import com.serefakyuz.meditation.ui.home.HomeRepository
import com.serefakyuz.meditation.ui.login.LoginRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

/**
 * Created by seref.akyuz on 7/29/2021
 */
@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient().newBuilder()
            .addInterceptor(loggingInterceptor)
            .build()


    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create()).build()

    @Singleton
    @Provides
    fun provideMeditationApi(retrofit: Retrofit) = retrofit.create(MeditationApi::class.java)

    @Singleton
    @Provides
    fun provideResponseHandler(@ApplicationContext appContext: Context) = ResponseHandler(appContext)

    @Singleton
    @Provides
    fun provideLoginRepository(preferenceManager: PreferenceManager) = LoginRepository(preferenceManager)

    @Singleton
    @Provides
    fun provideHomeRepository(api: MeditationApi, responseHandler: ResponseHandler) = HomeRepository(api, responseHandler)

}