package com.serefakyuz.meditation.di

import android.content.Context
import com.serefakyuz.meditation.repository.local.PreferenceManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by seref.akyuz on 7/29/2021
 */

@Module
@InstallIn(SingletonComponent::class)
class LocalRepositoryModule {

    @Singleton
    @Provides
    fun providePreferenceManager(@ApplicationContext appContext: Context) = PreferenceManager(appContext)
}