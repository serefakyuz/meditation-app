package com.serefakyuz.meditation.app

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by seref.akyuz on 7/27/2021
 */
@HiltAndroidApp
class MeditationApp: Application() {
    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
    }
}