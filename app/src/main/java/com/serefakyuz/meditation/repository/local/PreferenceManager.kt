package com.serefakyuz.meditation.repository.local

import android.content.Context
import android.content.SharedPreferences
import com.serefakyuz.meditation.ui.login.data.LoginRequestModel
import com.squareup.moshi.Moshi
import javax.inject.Inject

/**
 * Created by seref.akyuz on 7/29/2021
 */
class PreferenceManager @Inject constructor(private val context: Context) {
    companion object{
        private const val KEY_LOGIN_DATA = "loginData"
    }

    private var prefManager: SharedPreferences =
        context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    private var editor: SharedPreferences.Editor = prefManager.edit()

    fun cleanAllData(){
        editor.clear().commit()
    }

    fun cleanAuthData(){
        editor.remove(KEY_LOGIN_DATA).commit()
    }


    fun setLoginData(loginRequestModel: LoginRequestModel) =
        editor.putString(KEY_LOGIN_DATA, Moshi.Builder().build().adapter(LoginRequestModel::class.java).toJson(loginRequestModel)).commit()

    fun getLoginData(): LoginRequestModel? = prefManager.getString(KEY_LOGIN_DATA, null)?.let {
            Moshi.Builder().build().adapter(LoginRequestModel::class.java).fromJson(it)
        }?:null
}