package com.serefakyuz.meditation.repository.network

import com.serefakyuz.meditation.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Created by seref.akyuz on 7/29/2021
 */
class RetrofitClient {






    fun provideMeditationApi(retrofit: Retrofit) = retrofit.create(MeditationApi::class.java)
}