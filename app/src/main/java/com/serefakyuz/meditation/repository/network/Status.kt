package com.serefakyuz.meditation.repository.network

/**
 * Created by seref.akyuz on 7/29/2021
 */
enum class Status {
    SUCCESS,
    NO_STATE,
    ERROR,
    LOADING,
    UNAUTHORIZED
}