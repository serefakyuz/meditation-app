package com.serefakyuz.meditation.repository.network

import com.serefakyuz.meditation.ui.home.data.HomeDataResponseModel
import retrofit2.http.GET

/**
 * Created by seref.akyuz on 7/29/2021
 */
interface MeditationApi {
    companion object{
        const val PATH_HOME_FEED = "/api/jsonBlob/a07152f5-775c-11eb-a533-c90b9d55001f"
    }

    @GET(PATH_HOME_FEED)
    suspend fun getHomeData(): HomeDataResponseModel

}