package com.serefakyuz.meditation.repository.network

import android.content.Context
import com.serefakyuz.meditation.R
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception
import java.net.SocketTimeoutException

/**
 * Created by seref.akyuz on 7/29/2021
 */
const val ERROR_UNAUTHORIZED = "Unauthorized"
enum class ErrorCodes(val code: Int){
    SOCKET_TIMEOUT(-1),
    NO_NETWORK(-2),
    UNAUTHORIZED(401),
    NOT_FOUND(404),
    SERVER_ERROR(500)

}
open class ResponseHandler(val context: Context) {
    fun <T : Any> handleSuccess(data: T): Resource<T> = Resource.success(data)
    fun <T : Any> handleException(e: Exception): Resource<T> {
        return when (e) {
            is IOException -> {
                Resource.error(getErrorMessage(ErrorCodes.NO_NETWORK.code), null)
            }
            is HttpException -> Resource.error(getErrorMessage(e.code()), null)
            is SocketTimeoutException -> Resource.error(getErrorMessage(ErrorCodes.SOCKET_TIMEOUT.code), null)
            else -> Resource.error(getErrorMessage(Int.MAX_VALUE), null)
        }
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            ErrorCodes.SOCKET_TIMEOUT.code -> context.getString(R.string.errorTimeout)
            ErrorCodes.NO_NETWORK.code -> context.getString(R.string.networkErrorMsg)
            ErrorCodes.UNAUTHORIZED.code -> ERROR_UNAUTHORIZED
            ErrorCodes.NOT_FOUND.code -> context.getString(R.string.errorNotFound)
            ErrorCodes.SERVER_ERROR.code -> context.getString(R.string.errorServer)
            else -> context.getString(R.string.errorCommon)
        }
    }
}