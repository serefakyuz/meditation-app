package com.serefakyuz.meditation.repository.network

/**
 * Created by seref.akyuz on 7/29/2021
 */
data class Resource<out T>(var status: Status, val data: T?, val message: String?) {
    companion object{
        fun <T> success(data: T?): Resource<T> = Resource(Status.SUCCESS, data, null)
        fun <T> error(message: String?, data: T?): Resource<T> = Resource(if(message == ERROR_UNAUTHORIZED) Status.UNAUTHORIZED else Status.ERROR, data, message)
        fun <T> loading(data: T?): Resource<T> = Resource(Status.LOADING, data, null)
    }
}