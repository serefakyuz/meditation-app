package com.serefakyuz.meditation.ui.meditationdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.serefakyuz.meditation.databinding.FragmentHomeBinding
import com.serefakyuz.meditation.databinding.FragmentMeditationDetailBinding
import com.serefakyuz.meditation.ui.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by seref.akyuz on 7/30/2021
 */
@AndroidEntryPoint
class MeditationDetailFragment: BaseFragment() {

    private lateinit var binding: FragmentMeditationDetailBinding
    private val args: MeditationDetailFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMeditationDetailBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@MeditationDetailFragment
            item = args.playableItem
        }
        return binding.root
    }
}