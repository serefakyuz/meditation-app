package com.serefakyuz.meditation.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.serefakyuz.meditation.databinding.ItemStoryBinding
import com.serefakyuz.meditation.ui.home.data.IPlayable

/**
 * Created by seref.akyuz on 7/31/2021
 */
class StoriesAdapter(): ListAdapter<IPlayable, StoriesAdapter.StoryViewHolder>(MeditationDiffCallback())  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoryViewHolder =
        StoryViewHolder(ItemStoryBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: StoryViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    class StoryViewHolder(private val binding: ItemStoryBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(playableItem: IPlayable) {
            binding.apply {
                item = playableItem
                root.setOnClickListener {
                    it.findNavController().navigate(HomeFragmentDirections.actionHomeToDetail(playableItem))
                }
            }
        }
    }

    private class MeditationDiffCallback: DiffUtil.ItemCallback<IPlayable>(){
        override fun areItemsTheSame(oldItem: IPlayable, newItem: IPlayable): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: IPlayable, newItem: IPlayable): Boolean {
            return oldItem.getTitle() == newItem.getTitle()
        }

    }
}