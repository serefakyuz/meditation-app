package com.serefakyuz.meditation.ui.login

import androidx.lifecycle.MutableLiveData
import com.serefakyuz.meditation.R
import com.serefakyuz.meditation.repository.network.Status
import com.serefakyuz.meditation.ui.BaseViewModel
import com.serefakyuz.meditation.ui.login.data.LoginFormState
import com.serefakyuz.meditation.ui.login.data.LoginRequestModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by seref.akyuz on 7/29/2021
 */
@HiltViewModel
class LoginViewModel @Inject constructor(private val repository: LoginRepository): BaseViewModel(){
    val loginResponse = MutableLiveData<Boolean>()
    val loginFormState = MutableLiveData<LoginFormState>()

    fun login(username: String, password: String){
        requestStatus.value = Status.LOADING
        val loginData = LoginRequestModel(username, password)
        if(checkIsLoginDataValid(loginData)){
            loginResponse.value = repository.setUserData(loginData)
        }

    }

    private fun checkIsLoginDataValid(loginData: LoginRequestModel): Boolean{
        var result = false
        loginFormState.value  = if(!loginData.isUsernameValid()){
            LoginFormState(usernameError = R.string.invalid_username)
        }else if(!loginData.isPasswordValid()){
            LoginFormState(passwordError = R.string.invalid_password)
        }else{
            result = true
            LoginFormState(isDataValid = true)
        }
        return result
    }
}