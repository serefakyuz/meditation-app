package com.serefakyuz.meditation.ui.home.data

/**
 * Created by seref.akyuz on 7/31/2021
 */
data class HomeDataRequestModel(private val param: String)
