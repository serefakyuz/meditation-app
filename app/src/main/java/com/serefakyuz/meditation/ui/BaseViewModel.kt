package com.serefakyuz.meditation.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.serefakyuz.meditation.repository.network.Status

/**
 * Created by seref.akyuz on 7/29/2021
 */
open class BaseViewModel: ViewModel() {
    val requestStatus = MutableLiveData<Status>()
}