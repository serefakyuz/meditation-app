package com.serefakyuz.meditation.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.serefakyuz.meditation.repository.network.Resource
import com.serefakyuz.meditation.ui.BaseViewModel
import com.serefakyuz.meditation.ui.home.data.HomeDataRequestModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Created by seref.akyuz on 7/30/2021
 */
@HiltViewModel
class HomeViewModel @Inject constructor(private val repository: HomeRepository): BaseViewModel() {

    private val homeDataRequestModel = MutableLiveData<HomeDataRequestModel>()

    fun getHomeData(){
        homeDataRequestModel.value = HomeDataRequestModel("")
    }

    val homeDataResponse = homeDataRequestModel.switchMap {
        liveData {
            emit(Resource.loading(null))
            emit(repository.getHomeData())
        }
    }
}