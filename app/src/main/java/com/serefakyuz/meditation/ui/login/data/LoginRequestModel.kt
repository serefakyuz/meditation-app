package com.serefakyuz.meditation.ui.login.data

import com.squareup.moshi.JsonClass

/**
 * Created by seref.akyuz on 7/29/2021
 */
const val PASSWORD_VALIDATION_RULE = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,20}\$"

@JsonClass(generateAdapter = true)
data class LoginRequestModel(val username: String, val password: String){
    fun isUsernameValid(): Boolean{
        return username.length > 2
    }

    fun isPasswordValid(): Boolean{
        return PASSWORD_VALIDATION_RULE.toRegex().matches(password)
    }
}
