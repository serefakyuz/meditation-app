package com.serefakyuz.meditation.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.serefakyuz.meditation.R
import com.serefakyuz.meditation.databinding.ActivityLoginBinding
import com.serefakyuz.meditation.ui.MainActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {


    companion object{
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, LoginActivity::class.java))
        }
    }
    private lateinit var binding: ActivityLoginBinding
    private val loginViewModel: LoginViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.apply {
            lifecycleOwner = this@LoginActivity
            buttonLogin.setOnClickListener {
                this@LoginActivity.loginViewModel.login(inputETUsername.text.toString(), inputEditTextPassword.text.toString())
            }
        }

        loginViewModel.apply {
            loginFormState.observe(this@LoginActivity){
                val loginState = it ?: return@observe

                if(loginState.usernameError != null){
                    binding.inputLayoutUsername.error = getString(loginState.usernameError)
                }else{
                    binding.inputLayoutUsername.isErrorEnabled = false
                }
                if(loginState.passwordError != null){
                    binding.inputLayoutPassword.error = getString(loginState.passwordError)
                }else{
                    binding.inputLayoutPassword.isErrorEnabled = false
                }
            }
            loginResponse.observe(this@LoginActivity){
                val result = it ?: return@observe
                if(result){
                    MainActivity.startActivity(this@LoginActivity)
                    finish()
                }else{

                }
            }
        }
    }
}