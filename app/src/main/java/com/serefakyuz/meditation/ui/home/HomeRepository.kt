package com.serefakyuz.meditation.ui.home

import com.serefakyuz.meditation.repository.network.MeditationApi
import com.serefakyuz.meditation.repository.network.Resource
import com.serefakyuz.meditation.repository.network.ResponseHandler
import com.serefakyuz.meditation.ui.home.data.HomeDataResponseModel
import java.lang.Exception

/**
 * Created by seref.akyuz on 7/30/2021
 */
class HomeRepository(private val api: MeditationApi, private val responseHandler: ResponseHandler) {

    suspend fun getHomeData(): Resource<HomeDataResponseModel>{
        return try{
            val response = api.getHomeData()
            responseHandler.handleSuccess(response)
        }catch (e: Exception){
            responseHandler.handleException(e)
        }
    }
}