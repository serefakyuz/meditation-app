package com.serefakyuz.meditation.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.serefakyuz.meditation.ui.dialog.CommonDialogs

/**
 * Created by seref.akyuz on 7/30/2021
 */
open class BaseFragment: Fragment() {

    protected lateinit var commonDialog: CommonDialogs
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        commonDialog = CommonDialogs(requireContext())
    }
}