package com.serefakyuz.meditation.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.serefakyuz.meditation.R
import com.serefakyuz.meditation.databinding.FragmentHomeBinding
import com.serefakyuz.meditation.repository.network.Status
import com.serefakyuz.meditation.ui.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by seref.akyuz on 7/30/2021
 */
@AndroidEntryPoint
class HomeFragment: BaseFragment() {
    private lateinit var binding: FragmentHomeBinding
    private val viewModel: HomeViewModel by viewModels()
    private val meditationsAdapter = MeditationsAdapter()
    private val storiesAdapter = StoriesAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@HomeFragment
            viewModel = this@HomeFragment.viewModel

            recyclerViewMeditations.adapter = meditationsAdapter
            recyclerViewStories.adapter = storiesAdapter
        }
        viewModel.apply {
            getHomeData()
            homeDataResponse.observe(viewLifecycleOwner){
                when(it.status){
                    Status.SUCCESS ->{
                        it.data?.let { response->
                            meditationsAdapter.submitList(response.meditations)
                            storiesAdapter.submitList(response.stories)
                        }
                    }
                    Status.ERROR->{
                        commonDialog.showErrorDialog(it.message!!)
                    }
                    Status.UNAUTHORIZED->{

                    }
                }
            }
        }
        return binding.root
    }
}