package com.serefakyuz.meditation.ui.login.data

/**
 * Created by seref.akyuz on 7/29/2021
 */
data class LoginFormState( val usernameError: Int? = null,
                           val passwordError: Int? = null,
                           val isDataValid: Boolean = false)
