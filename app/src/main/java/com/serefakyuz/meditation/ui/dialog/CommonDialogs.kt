package com.serefakyuz.meditation.ui.dialog

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog
import com.serefakyuz.meditation.R

/**
 * Created by seref.akyuz on 7/31/2021
 */
class CommonDialogs(private val context: Context) {
    private var dialog: MaterialDialog? = null


    fun showErrorDialog( message: String, title: String? = null){
        dialog?.let {
            it.title(title?.let { null }?: R.string.error, title)
            it.message(null, message)
            it.show()
        }?:run{
            dialog = MaterialDialog(context).show {
                title(title?.let { null }?: R.string.error, title)
                message(null, message)
                positiveButton(R.string.ok)
                cancelable(true)
            }
        }
    }
}