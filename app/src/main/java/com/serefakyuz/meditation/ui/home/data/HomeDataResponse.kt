package com.serefakyuz.meditation.ui.home.data


import android.os.Parcelable
import com.serefakyuz.meditation.utils.DateUtils
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import java.lang.NumberFormatException

@Parcelize
@JsonClass(generateAdapter = true)
data class MeditationsItem(@Json(name = "image")
                           val image_: Image?,
                           @Json(name = "releaseDate")
                           val releaseDate_: String?,
                           @Json(name = "subtitle")
                           val subtitle_: String? = "",
                           @Json(name = "title")
                           val title_: String? = "",
                           @Json(name = "content")
                           val content_: String? = ""): IPlayable{
    override fun getTitle(): String? = title_

    override fun getSubInfo(): String? = subtitle_

    override fun getImage(): Image? = image_

    override fun getText(): String? = content_

    override fun getTs(): Long? {
        return try {
            releaseDate_?.toLong()?.times(1000)
        }catch (e: NumberFormatException){
            null
        }
    }

    override fun getDate(): String? {
        return getTs()?.let {
            DateUtils.getFormattedDate(it)
        }?:run{
            null
        }
    }

}

@Parcelize
@JsonClass(generateAdapter = true)
data class StoriesItem(@Json(name = "date")
                       val date_: String? = "",
                       @Json(name = "image")
                       val image_: Image?,
                       @Json(name = "name")
                       val name_: String? = "",
                       @Json(name = "text")
                       val text_: String? = "",
                       @Json(name = "category")
                       val category_: String? = ""): IPlayable{
    override fun getTitle(): String? = name_

    override fun getSubInfo(): String? = category_

    override fun getImage(): Image? = image_

    override fun getText(): String? = text_

    override fun getTs(): Long? {
        return try {
            date_?.toLong()?.times(1000)
        }catch (e: NumberFormatException){
            null
        }
    }

    override fun getDate(): String? {
        return getTs()?.let {
            DateUtils.getFormattedDate(it)
        }?:run{
            null
        }
    }
}

@Parcelize
@JsonClass(generateAdapter = true)
data class Image(@Json(name = "small")
                 val small: String? = "",
                 @Json(name = "large")
                 val large: String? = ""): Parcelable

@JsonClass(generateAdapter = true)
data class HomeDataResponseModel(@Json(name = "isBannerEnabled")
                                 val isBannerEnabled: Boolean? = false,
                                 @Json(name = "stories")
                                 val stories: List<StoriesItem>?,
                                 @Json(name = "meditations")
                                 val meditations: List<MeditationsItem>?)

interface IPlayable : Parcelable{
    fun getTitle(): String?
    fun getSubInfo(): String?
    fun getImage(): Image?
    fun getText(): String?
    fun getTs(): Long?
    fun getDate(): String?
}


