package com.serefakyuz.meditation.ui.login

import com.serefakyuz.meditation.repository.local.PreferenceManager
import com.serefakyuz.meditation.ui.login.data.LoginRequestModel

/**
 * Created by seref.akyuz on 7/29/2021
 */
class LoginRepository(private val preferenceManager: PreferenceManager) {

    fun setUserData(loginRequestModel: LoginRequestModel) = preferenceManager.setLoginData(loginRequestModel)
    fun getUserData() = preferenceManager.getLoginData()
}